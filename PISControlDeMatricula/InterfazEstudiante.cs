﻿using PISControlDeMatricula.Modelos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PISControlDeMatricula
{
    class InterfazEstudiante
    {
        public void InformacionPersonal(string correo)
        {
            Console.Clear();
            using (var db = new SistemaContext())
            {
                var estudiante = (from resultado in db.estudiantes
                                  where resultado.Correo == correo
                                  select resultado).FirstOrDefault<Estudiante>();

                Console.Write("NOMBRES: {0}  NÚMERO DE CÉDULA:{2}\nAPELLIDOS: {1}  CARRERA: {3} \n"
                    , estudiante.Nombre, estudiante.Apellido, estudiante.Cedula, estudiante.Carrera);

            }

        }
        public void InformacionMatricula(string correo)
        {
            Console.Clear();
            using (var db = new SistemaContext())
            {
                var estudiante = (from resultado in db.estudiantes
                                  where resultado.Correo == correo
                                  select resultado).FirstOrDefault<Estudiante>();

                Console.WriteLine("Nombres: {0} {1}\n número de cédula: {3}\n Carrera: {2}\n Estado de matrícula: {4}\n ", estudiante.Apellido,
                    estudiante.Nombre, estudiante.Carrera, estudiante.Cedula, estudiante.Estado);

                var listaDocumentos = (from resultado in db.documentos
                                       where resultado.IdNivel == estudiante.IdNivel
                                       select resultado).ToList();

                Console.WriteLine("Lista de requisitos necesarios para legalizar su matrícula: ");
                foreach (var requisito in listaDocumentos)
                {
                    Console.WriteLine( "\t" + requisito.NombreDocumento);
                }
                if (estudiante.Estado=="Legalizado")
                {
                    Console.WriteLine("\nFELICITACIONES, USTED HA LEGALIZADO SU MATRÍCULA!");
                }
                else
                {
                    if (estudiante.Estado=="No Legalizado" || estudiante.Estado=="Pendiente")
                    {
                        Console.WriteLine("\nPOR FAVOR, ACERCARCE A SECRETARÍA A LEGALIZAR SU MATRÍCULA");
                    }
                   
                }
            }

        }

        public void ModificarContrasenia()
        {
            Console.Clear();
            using (var db = new SistemaContext())
            {
                string contrasenia = "";
                Console.WriteLine("ingrese la contraseña actual");
                //EnmascararContrasenia();
                contrasenia = EnmascararContrasenia();
                //contrasenia = Console.ReadLine();
                var estudia = (from resultado in db.estudiantes
                             where resultado.Contraseña == contrasenia
                             select resultado).FirstOrDefault();
                if (estudia == null)
                {
                    Console.WriteLine("contraseña incorrecta");
                    ModificarContrasenia();
                }
                else
                {
                    string contrasenia1 = "";
                    string contrasenia2 = "";
                    Estudiante estudiantes = db.estudiantes.Find(estudia.Id);
                    Console.Write("Ingrese nueva contraseña: ");
                    contrasenia1 = EnmascararContrasenia();
                    Console.Write("Vuelva a ingresar nueva contraseña: ");
                    contrasenia2 = EnmascararContrasenia();
                    if (contrasenia1 == contrasenia2)
                    {
                        estudiantes.Contraseña = contrasenia1;
                        db.SaveChanges();

                    }
                    else
                    {
                        Console.WriteLine("las cotraseñas no coinciden");
                        ModificarContrasenia();
                    }


                }
            }
        }
        private string EnmascararContrasenia()
        {
            ConsoleKeyInfo ultimaTecla;
            bool continuar;
            char mostrar;
            char[] cadena = new char[100];
            int i = 0;
            continuar = true;
            mostrar = '*';
            //string correoIngresado = "";
            string contraseniaIngresada = "";
            Console.WriteLine();

            while (continuar)
            {

                ultimaTecla = Console.ReadKey(true);

                if (ultimaTecla.KeyChar != 13)
                {

                    if (ultimaTecla.KeyChar != 8)
                    {
                        cadena[i] = ultimaTecla.KeyChar;
                        i++;
                        Console.Write(mostrar);
                    }
                    else
                    {
                        cadena[i] = '\0';
                        i--;
                        Console.Write("\b \b");
                        continuar = true;
                    }

                }
                else
                    continuar = false;
            }
            cadena[i] = '\0';

            for (int j = 0; j < cadena.Length; j++)
            {
                if (cadena[j].Equals('\0'))
                {
                    break;
                }
                else
                {
                    contraseniaIngresada = contraseniaIngresada + cadena[j];
                }

            }
            return contraseniaIngresada;
        }
    }
}
