﻿using PISControlDeMatricula.Modelos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PISControlDeMatricula
{
    class InterfazAdministrador
    {
        //Menu menu = new Menu();
        public void RegistroEstudiante()
        {
            
                using (var db = new SistemaContext())
                {
                    
                    Console.WriteLine("Registro de estudiantes\n");
                    Estudiante estudiante = new Estudiante();
                    Console.Write("nombres: ");
                    estudiante.Nombre = Console.ReadLine();
                    Console.Write("apellidos: ");
                    estudiante.Apellido = Console.ReadLine();
                    Console.Write("numero de cédula: ");
                    estudiante.Cedula = Console.ReadLine();
                    Console.Write("carrera: ");
                    estudiante.Carrera = Console.ReadLine();
                    Console.Write("nivel (escriba el número correspondiente al nivel ej: primer nivel tendria el número 1): ");
                    estudiante.IdNivel = int.Parse(Console.ReadLine());
                    Console.Write("correo: ");
                    estudiante.Correo = Console.ReadLine();
                    Console.Write("contraseña: ");
                    estudiante.Contraseña = Console.ReadLine();
                    estudiante.Estado = "No legalizado";
                    Console.WriteLine(estudiante.Id);
                    db.Add(estudiante);
                    db.SaveChanges();

                }

            return;

        }
        //public void ModificarEstudiante()
        //{

        //}

        public void RegistroDocumento()
        {

            Console.WriteLine("Buscar");
            Console.Write("ingrese número de cedula del estudiante: ");
            string opcion = Console.ReadLine();
            using (var db = new SistemaContext())
            {
                Console.Clear();
                var estudiante = (from resultado in db.estudiantes
                                  where resultado.Cedula == opcion
                                  select resultado).FirstOrDefault();
                if (estudiante == null)
                {
                    Console.WriteLine("no se encotraron resultados ");
                }
                else
                {
                    Console.WriteLine("{4} {3} {0} {1} {2} ", estudiante.Apellido, estudiante.Nombre, estudiante.Carrera, estudiante.Cedula, estudiante.Id);
                
               


                    Console.Write("\ndocumentos disponibles de registro:\n");
                    var documento = (from resultado in db.documentos
                                 where resultado.IdNivel == estudiante.IdNivel
                                 select resultado).ToList<Documento>();

                    Console.WriteLine("Id\t Documentos ");
                    foreach (var requisito in documento)
                    {
                        Console.WriteLine(requisito.Id+"\t "+requisito.NombreDocumento);
                    }

                    DocumentosPresentados documentosPresentados = new DocumentosPresentados();

                    MenuIngreso(documentosPresentados, estudiante);
                }
            }
        }
        private void MenuIngreso(DocumentosPresentados documentos, Estudiante estudiante)
        {
            int opcionRegistro = 0;
            Console.WriteLine("1.- Registrar documento \n2.-Finalizar/generar reporte ");
            Console.Write("ingrese su opcion: ");
            try
            {
                opcionRegistro = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ha ingresado un tipo de dato no permitido, a continuación ingrese un número que represente su opcion");
            }
            finally
            {
                opcionRegistro = int.Parse(Console.ReadLine());
            }
            switch (opcionRegistro)
            {
                case 1:
                    AsignacionDocumento(documentos, estudiante);
                    MenuIngreso(documentos, estudiante);

                    break;
                case 2:
                    
                    CrearReporte(estudiante);

                    break;
                default:
                    MenuIngreso(documentos, estudiante);
                    break;
            }

        }
        private void AsignacionDocumento(DocumentosPresentados documento, Estudiante estudiante)
        {
            using (var db = new SistemaContext())
            {

                Console.WriteLine("Ingresde Id del Documento a registrar:");
                int docId = int.Parse(Console.ReadLine());
                var doc = (from resultado in db.documentos
                           where resultado.Id == docId
                           select resultado).FirstOrDefault();
                documento = new DocumentosPresentados();
                documento.NombreDocumento = doc.NombreDocumento;
                documento.IdEstudiante = estudiante.Id;


                var document = (from resultado in db.ListDocumentosPresentados
                                where resultado.IdEstudiante == estudiante.Id
                                select resultado).ToList();
                int ban = 0;
                for (int i=0; i<document.Count; i++)
                {
                    if (document[i].NombreDocumento == doc.NombreDocumento)
                    {
                        ban = 1;
                        break;
                    }

                }
                if (ban == 1)
                {
                    Console.WriteLine("El documento ya ha sido registrado");
                }
                else
                {
                    db.Add(documento);
                    db.SaveChanges();
                }
            }

        }
        public void VerDocumentos()
        {
            Console.Clear();
            Console.WriteLine("Buscar");
            Console.Write("ingrese número de cedula del estudiante: ");
            string opcion = Console.ReadLine();
            using (var db = new SistemaContext())
            {
                Console.Clear();
                var estudiante = (from resultado in db.estudiantes
                                  where resultado.Cedula == opcion
                                  select resultado).FirstOrDefault();
                if (estudiante == null)
                {
                    Console.WriteLine("no se encontraron resultados ");
                }
                else
                {
                    var doc = (from resultado in db.ListDocumentosPresentados
                               where resultado.IdEstudiante == estudiante.Id
                               select resultado).ToList();
                    if (doc==null)
                    {
                        Console.WriteLine("no se encontraron Documentos ");

                        Console.WriteLine("\nEstado: " + estudiante.Estado);
                    }
                    else
                    {
                        Console.WriteLine("{4} {3} {0} {1} {2} ", estudiante.Apellido, estudiante.Nombre, estudiante.Carrera, estudiante.Cedula, estudiante.Id);
                        Console.WriteLine("\ndocumentos presentados: \n");

                        foreach (var item in doc)
                        {
                        Console.WriteLine("\t"+item.NombreDocumento);
                        }

                        Console.WriteLine("\nEstado: "+estudiante.Estado);
                    }
                    
                }

            }
        }
        public void VisualizarReportes()
        {
            Console.Clear();
            using (var db = new SistemaContext())
            {
                int opcion = 0;
                Console.WriteLine(" 1.- ver todos los estudiantes\n 2.- ver estudiantes legalizados\n  3.- ver estudiantes no legalizados");
                Console.Write("ingrese su opcion: ");
                try
                {
                    opcion = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ha ingresado un tipo de dato no permitido, a continuación ingrese un número que represente su opcion");
                }
                finally
                {
                    opcion = int.Parse(Console.ReadLine());
                }
                switch (opcion)
                {
                    case 1:
                        List<Estudiante> Listreportes = db.estudiantes.ToList();
                        foreach (var student in Listreportes)
                        {
                        Console.WriteLine("{4} {3}\t {0} {1}\t {2}\t {5}", student.Apellido, student.Nombre,
                        student.Carrera, student.Cedula, student.Id, student.Estado); ;
                        }
                        break;
                    case 2:
                        var estudiante = (from resultado in db.estudiantes
                                   where resultado.Estado == "Legalizado"
                                   select resultado).ToList();
                       
                        foreach (var student in estudiante)
                        {               
                           Console.WriteLine("{4} {3}\t {0} {1}\t {2}\t {5}", student.Apellido, student.Nombre,
                           student.Carrera, student.Cedula, student.Id, student.Estado); ;    
                        }
                        break;
                    case 3:
                        var estudiane = (from resultado in db.estudiantes
                                          where resultado.Estado == "No Legalizado"
                                          select resultado).ToList();

                        var estudiantes = (from resultado in db.estudiantes
                                         where resultado.Estado == "Pendiente"
                                         select resultado).ToList();
                        if (estudiane == null && estudiantes == null)
                        {
                            Console.WriteLine("no se encontraron resultados");
                        }

                        foreach (var student in estudiane)
                        {  
                           Console.WriteLine("{4} {3}\t {0} {1}\t {2}\t {5}", student.Apellido, student.Nombre,
                           student.Carrera, student.Cedula, student.Id, student.Estado);                             
                        }
                        foreach (var student in estudiantes)
                        {
                            Console.WriteLine("{4} {3}\t {0} {1}\t {2}\t {5}", student.Apellido, student.Nombre,
                            student.Carrera, student.Cedula, student.Id, student.Estado);
                        }
                        break;
                    default:
                        VisualizarReportes();
                        break;
                }
                
            }

        }
        public void CrearReporte(Estudiante estudiante)
        {
            using (var db = new SistemaContext())
            {
                var document = (from resultado in db.ListDocumentosPresentados
                                where resultado.IdEstudiante == estudiante.Id
                                select resultado).FirstOrDefault();

                

                if (document == null)
                {
                    var estudiant = db.estudiantes.Find(estudiante.Id);
                    estudiante.Estado = "No Legalizado";

                    db.SaveChanges();
                }
                else
                {
                    var documento = (from resultado in db.ListDocumentosPresentados
                                    where resultado.IdEstudiante == estudiante.Id
                                    select resultado).ToList();

                    var doc = (from resultado in db.documentos
                           where resultado.IdNivel == estudiante.IdNivel
                           select resultado).ToList();


                    if (documento.Count<doc.Count)
                    {
                        var estudiant = db.estudiantes.Find(estudiante.Id);
                        estudiant.Estado = "Pendiente";

                        db.SaveChanges();
                    }
                    else
                    {
                        if (documento.Count == doc.Count)
                        {
                            var estudiant = db.estudiantes.Find(estudiante.Id);
                            estudiant.Estado = "Legalizado";

                            db.SaveChanges();
                        }
                    }

                }

            }

        }
        public void ModificarContrasenia()
        {
            Console.Clear();
            using (var db = new SistemaContext())
            {
                string contrasenia = "";
                Console.Write("ingrese la contraseña actual: ");
                contrasenia = EnmascararContrasenia();
                var admin = (from resultado in db.administradors
                             where resultado.Contraseña == contrasenia
                             select resultado).FirstOrDefault<Administrador>();
                if (admin==null)
                {
                    Console.WriteLine("contraseña incorrecta");
                    ModificarContrasenia();
                }
                else
                {
                    string contrasenia1 = "";
                    string contrasenia2 = "";
                    Administrador administrador = db.administradors.Find(admin.Id);
                    Console.Write("\nIngrese nueva contraseña: ");
                    contrasenia1 = EnmascararContrasenia();
                    Console.Write("\nVuelva a ingresar nueva contraseña: ");
                    contrasenia2 = EnmascararContrasenia();
                    if (contrasenia1 == contrasenia2)
                    {
                        administrador.Contraseña = contrasenia1;
                        db.SaveChanges();

                    }
                    else
                    {
                        Console.WriteLine("las cotraseñas no coinciden");
                        ModificarContrasenia();
                    }


                }
               
                
            }
        }
        public string GenerarPassword(int longitud)
        {
            string contrasenia = string.Empty;
            string[] caracteres = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            Random EleccionAleatoria = new Random();

            for (int i = 0; i < longitud; i++)
            {
                int LetraAleatoria = EleccionAleatoria.Next(0, 100);
                int NumeroAleatorio = EleccionAleatoria.Next(0, 9);

                if (LetraAleatoria < caracteres.Length)
                {
                    contrasenia += caracteres[LetraAleatoria];
                }
                else
                {
                    contrasenia += NumeroAleatorio.ToString();
                }
            }
            return contrasenia;
        }
        private string EnmascararContrasenia()
        {
            ConsoleKeyInfo ultimaTecla;
            bool continuar;
            char mostrar;
            char[] cadena = new char[100];
            int i = 0;
            continuar = true;
            mostrar = '*';
            //string correoIngresado = "";
            string contraseniaIngresada = "";
            Console.WriteLine();

            while (continuar)
            {

                ultimaTecla = Console.ReadKey(true);

                if (ultimaTecla.KeyChar != 13)
                {

                    if (ultimaTecla.KeyChar != 8)
                    {
                        cadena[i] = ultimaTecla.KeyChar;
                        i++;
                        Console.Write(mostrar);
                    }
                    else
                    {
                        cadena[i] = '\0';
                        i--;
                        Console.Write("\b \b");
                        continuar = true;
                    }

                }
                else
                    continuar = false;
            }
            cadena[i] = '\0';

            for (int j = 0; j < cadena.Length; j++)
            {
                if (cadena[j].Equals('\0'))
                {
                    break;
                }
                else
                {
                    contraseniaIngresada = contraseniaIngresada + cadena[j];
                }

            }
            return contraseniaIngresada;
        }
    }
}
