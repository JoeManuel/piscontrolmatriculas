﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
//using SistemaDeMatricula.Migrations;
using PISControlDeMatricula.Modelos;
using PISControlDeMatricula.Migrations;

namespace PISControlDeMatricula
{
    class Menu
    {
        InterfazAdministrador InterfazAdmin = new InterfazAdministrador();
        InterfazEstudiante interfazEstudiante = new InterfazEstudiante();

        public void Login()
        {
            ConsoleKeyInfo ultimaTecla;
            bool continuar;
            char mostrar;
            char[] cadena = new char[100];
            int i = 0;
            continuar = true;
            mostrar = '*';
            string correoIngresado = "";
            string contraseniaIngresada = "";
            Console.WriteLine();
           
                Console.WriteLine("===Iniciar Sesión===");
                Console.Write("usuario: ");
                correoIngresado = Console.ReadLine();
                Console.Write("cotraseña: ");
                

                while (continuar)
                {

                    ultimaTecla = Console.ReadKey(true);

                    if (ultimaTecla.KeyChar != 13)
                    {
                        
                        if (ultimaTecla.KeyChar != 8)
                        {
                            cadena[i] = ultimaTecla.KeyChar;
                            i++;
                            Console.Write(mostrar);
                        }
                        else
                        {
                            cadena[i] = '\0';
                            i--;
                            Console.Write("\b \b");
                            continuar = true;
                        }

                    }
                    else
                        continuar = false;
                }
                cadena[i] = '\0';

                for (int j = 0; j < cadena.Length; j++)
                {
                    if (cadena[j].Equals('\0'))
                    {
                        break;
                    }
                    else
                    {
                        contraseniaIngresada = contraseniaIngresada + cadena[j];
                    }

                }
                Console.Clear();

                using (var db = new SistemaContext())
                {
                    var estudiante = (from resultado in db.estudiantes
                                      where resultado.Correo == correoIngresado
                                      select resultado).FirstOrDefault<Estudiante>();


                    if (estudiante == null)
                    {
                        //Console.WriteLine("usuario o cotraseña inválidas");
                        //Login();
                        var administrador = (from resultado in db.administradors
                                             where resultado.Correo == correoIngresado
                                             select resultado).FirstOrDefault<Administrador>();
                        if (administrador == null)
                        {

                            Console.WriteLine("usuario o cotraseña inválidas");
                            Login();
                        }
                        else
                        {
                            if (administrador.Contraseña == contraseniaIngresada)
                            {
                                Console.Clear();
                                //mostrar menú con opciones del administrador como ingresar estudiantes al sistema, gestionar documento y realizar reportes 
                                Console.WriteLine("administrador");
                                MenuAdministrador(correoIngresado);
                            }
                            else
                            {

                                Console.WriteLine("\nusuario o cotraseña inválidas");
                                Login();


                            }

                        }

                    }
                    else
                    {
                        if (estudiante.Contraseña == contraseniaIngresada)
                        {
                            //mostrar menú con opciones del estudiante como ver información personal y sobre estado legal 
                            Console.Clear();
                            Console.WriteLine("Estudiante\n");
                            MenuEstudiante(correoIngresado);
                        }
                        else
                        {
                            Console.WriteLine("usuario o cotraseña inválidas");
                            Login();
                        }

                    }

                }

        }

        public void MenuAdministrador(string correo)
        {
            int opcion = 0;
            AdminInfo();
            Console.WriteLine("1.- Ingresar Estudiante \n2.- Registrar documentos del estudiante" +
                "\n3.- Visualizar reportes \n4.- Ver documentos Presentados \n5.- Cambiar contraseña \n6.- salir");
            Console.Write("ingrese su opcion: ");
            try
            {
                 opcion = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ha ingresado un tipo de dato no permitido, a continuación ingrese un número que represente su opcion");
            }
            finally
            {
                opcion = int.Parse(Console.ReadLine());
            }
            
            switch (opcion)
            {
                case 1:
                    
                    Console.Clear();
                        InterfazAdmin.RegistroEstudiante();
                        VolverMenuAdministrador(correo);
                    
                    
                    break;

                case 2:
                    Console.Clear();
                    InterfazAdmin.RegistroDocumento();
                    VolverMenuAdministrador(correo);
                    break;
                case 3:
                    InterfazAdmin.VisualizarReportes();
                    VolverMenuAdministrador(correo);
                    break;
                case 4:
                    InterfazAdmin.VerDocumentos();
                    VolverMenuAdministrador(correo);
                    break;
                case 5:
                    InterfazAdmin.ModificarContrasenia();
                    VolverMenuAdministrador(correo);
                    break;
                case 6:
                    Console.Clear();
                    Login();
                    break;
                default:

                    MenuAdministrador(correo);
                    break;
            }


        }
        public void MenuEstudiante(string correo)
        {
            EstudianteInfo(correo);
            int opcion = 0;
            Console.WriteLine("1.- Ver información personal \n2.- Ver estado de matrícula" +
                "\n3.- Cambiar contraseña  \n4.- salir");
            Console.Write("ingrese su opcion: ");
            try
            {
                opcion = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ha ingresado un tipo de dato no permitido, a continuación ingrese un número que represente su opcion");
            }
            finally
            {
                opcion = int.Parse(Console.ReadLine());
            }

            switch (opcion)
            {
                case 1:
                    interfazEstudiante.InformacionPersonal(correo);
                    VolverMenuEstudiante(correo);
                    break;

                case 2:
                    interfazEstudiante.InformacionMatricula(correo);
                    VolverMenuEstudiante(correo);
                    break;
                case 3:
                    interfazEstudiante.ModificarContrasenia();
                    VolverMenuEstudiante(correo);
                    break;
                case 4:
                    Console.Clear();
                    Login();
                    break;
                default:
                    Console.Clear(); 
                    MenuEstudiante(correo);
                    break;
            }
        }
        //
        public void AdminInfo()
        {
            using (var db = new SistemaContext())
            {
                List<Administrador> ListAdministrador = db.administradors.ToList();
                foreach (var admin in ListAdministrador)
                {
                    Console.WriteLine(admin.Nombre + " " + admin.Apellido + " \n");
                }
            }


        }

        public void EstudianteInfo(string correo)
        {
            using (var db = new SistemaContext())
            {
                var estudiante = (from resultado in db.estudiantes
                                  where resultado.Correo == correo
                                  select resultado).FirstOrDefault<Estudiante>();

                Console.WriteLine(estudiante.Nombre + " " + estudiante.Apellido + " \n");

            }


        }
        public void VolverMenuAdministrador(string correo)
        {
            int opcion = 0;
            Console.WriteLine("\npara volver presione 1");
            try
            {
                opcion = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ha ingresado un tipo de dato no permitido, a continuación ingrese un número que represente su opcion");
            }
            finally
            {
                opcion = int.Parse(Console.ReadLine());
            }
            if (opcion == 1)
            {
                Console.Clear();
                MenuAdministrador(correo);
            }
            else
            {
                Console.Clear();
                VolverMenuAdministrador(correo);
            }
        }
        public void VolverMenuEstudiante(string correo)
        {
            int opcion = 0;
            Console.WriteLine("\npara volver presione 1");
            try
            {
                opcion = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ha ingresado un tipo de dato no permitido, a continuación ingrese un número que represente su opcion");
            }
            finally
            {
                opcion = int.Parse(Console.ReadLine());
            }
            if (opcion == 1)
            {
                Console.Clear();
                MenuEstudiante(correo);
            }
            else
            {
                Console.Clear();
                VolverMenuEstudiante(correo);
            }
        }

    }
}
