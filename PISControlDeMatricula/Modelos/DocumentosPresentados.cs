﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PISControlDeMatricula.Modelos
{
    class DocumentosPresentados
    {
        public int Id { get; set; }
        public string NombreDocumento { get; set; }
        public int IdEstudiante { get; set; }
    }
}
